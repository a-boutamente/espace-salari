$(document).ready(function() {
    $('[data-toggle="popover"]').popover()
    $('.managers-header').on('click', function(e) {
        e.preventDefault();
        $('.managers-row').removeClass('is-active');
        $('.managers-header').fadeIn();
        $(this).fadeOut();
        $(this).parent().addClass('is-active');
    });
    $('.pro-exp-more').on('click', function(e) {
        e.preventDefault();
        $(this).fadeOut();
        $(this).closest('.pro-exp-item').css('height', '100%');
    });
    $('body').on('click', '.popover-close', function(e) {
        e.preventDefault();
        $('.check-item-alert').popover('hide');
    });
    $('.selectpicker').selectpicker();
    $('#dateStart').datepicker();
    $('#dateEnd').datepicker();
    $('#dateSinistre').datepicker();
    // mobile toggel 
    $('.menu-moble-toggel').on('click',function (e) { 
        e.preventDefault();
        $('#sidebarMenu').addClass('active');
        $('body').append('<div class="back-filter"></div>');
    })
    $('.menu-close').on('click',function (e) { 
        e.preventDefault();
        $('#sidebarMenu').removeClass('active');
        $('.back-filter').remove();
    })
    $('.flat-menu-toggel').not('.modal-pop').on('click',function (e) { 
        e.preventDefault();
        $('#topMenuMobile').addClass('active');
        $('body').append('<div class="back-filter"></div>');
    })
    $('.top-menu-close').on('click',function (e) { 
        e.preventDefault();
        $('#topMenuMobile').removeClass('active');
        $('.back-filter').remove();
    })
});